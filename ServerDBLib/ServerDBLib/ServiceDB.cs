﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using KirasaUModelsDBLib;
using OperationsTablesLib;
using InheritorsEventArgs;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace ServerDBLib
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Single, EnsureOrderedDispatch = true, UseSynchronizationContext = false)] 
    public partial class ServiceDB : IServiceDB
    {
        #region Events
        public static event EventHandler<OperationTableEventArgs> OnReceiveMsgDB;
        public static event EventHandler<OperationTableEventArgs> OnReceiveErrorDB;

        public static event EventHandler<ServerEventArgs> OnStateClient;
        public static event EventHandler<ServerEventArgs> OnErrorClient;

        public static event EventHandler<string> OnMessToHost;
        #endregion

        #region Clients
        private static ConcurrentDictionary<int, ClientService> clients = new ConcurrentDictionary<int, ClientService>();
        private static object clientLocker = new object();
        private static int nextId = 1;
        #endregion

        /// <summary>
        /// словарь, который хранит String - имя таблицы, 
        /// ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей) 
        /// </summary>
        private static Dictionary<NameTable, ITableAction> dicOperTables;


        public ServiceDB()
        {
            if (dicOperTables != null)
                return;

            Operation.OnReceiveData += HandlerUpData;
            Operation.OnAddRange += HandlerAddRange;
            Operation.OnReceiveRecord += HandlerUpRecord;
            Operation.OnSendMessToHost += HandlerMessToHostOperation;

            dicOperTables = new Dictionary<NameTable, ITableAction>
            {
                { NameTable.TableSectorsRecon, new OperationTableDb<TableSectorsRecon>()},
                { NameTable.TableFreqForbidden, new OperationTableDb<TableFreqForbidden>()},
                { NameTable.TableFreqRangesRecon, new OperationTableDb<TableFreqRangesRecon>()},
                { NameTable.GlobalProperties, new OperationGlobalProperties() },
                { NameTable.TableRes, new OperationTempTable<TableRes>()},
                { NameTable.TableResPattern, new OperationTableDb<TableResPattern>()},
                { NameTable.TableResArchive, new OperationTableResArchive()},
                { NameTable.TableTrackArchive, new OperationTableDb<TableTrackArchive>()},
                { NameTable.TableRemotePoints, new OperationTableDb<TableRemotePoints>()},
                { NameTable.TableResDistribution, new OperationTempResDistribution()},
                { NameTable.TableTrackDistribution, new OperationTempTrackDistribution()}
            };
        }


        public static void TryDisconnectClients()
        {
            if (clients.Count == 0)
                return;
            foreach (var client in clients.Values)
            {
                try
                {
                    client.OperContext.GetCallbackChannel<IServerCallback>().Abort();
                }
                catch
                {
                    continue;
                }
            }
        }


        public int Connect(string name)
        {
            lock (clientLocker)
            {
                ClientService client = new ClientService()
                {
                    ID = nextId,
                    Name = name,
                    OperContext = OperationContext.Current
                };
                nextId++;

                clients.TryAdd(client.ID, client);
                SendStateClient(client.Name, ServerEventArgs.ActClient.Connect);

                return client.ID;
            }
        }

        public void Disconnect(int id)
        {
            if (clients.ContainsKey(id))
            {
                var result = clients.TryRemove(id, out ClientService removedClient);
                if (result)
                    SendStateClient(removedClient.Name, ServerEventArgs.ActClient.Disconnect);
            }
        }

        public bool Ping(int Id)
        {
            if (clients.ContainsKey(Id))
            {
                return true;
            }
            return false;
        }

        private void SendStateClient(string ClientName, ServerEventArgs.ActClient act)
        {
            OnStateClient?.Invoke(this, new ServerEventArgs(DateTime.Now.ToShortTimeString(), ClientName, act));
        }

        private void SendErrorOfClient(Errors.EnumServerError error, string Mess)
        {
            OnErrorClient?.Invoke(this, new ServerEventArgs(DateTime.Now.ToShortTimeString(), error, Mess));
        }

        private void SendMessToHost(int idClient, NameTableOperation act, string Mess)
        {
            OnReceiveMsgDB?.Invoke(this, new OperationTableEventArgs(act, Errors.EnumDBError.None, Mess));
        }

        private void SendMessToHost(int idClient, NameChangeOperation act, string Mess)
        {
            OnReceiveMsgDB?.Invoke(this, new OperationTableEventArgs(act, Errors.EnumDBError.None, Mess));
        }

        private void SendError(InheritorsException.ExceptionLocalDB error, NameTableOperation operation)
        {
            OperationTableEventArgs eventArgs = new OperationTableEventArgs(operation, error.Error, error.Message);

            SendErrorToHost(eventArgs);

            if (clients.ContainsKey(error.IdClient))
            {
                clients[error.IdClient].OperContext.GetCallbackChannel<IServerCallback>().ErrorCallback(eventArgs);
                return;
            }
        }

        private void SendError(InheritorsException.ExceptionLocalDB error, NameChangeOperation operation)
        {
            OperationTableEventArgs eventArgs = new OperationTableEventArgs(operation, error.Error, error.Message);

            SendErrorToHost(eventArgs);

            if (clients.ContainsKey(error.IdClient))
            {
                clients[error.IdClient].OperContext.GetCallbackChannel<IServerCallback>().ErrorCallback(eventArgs);
                return;
            }
        }

        private void SendErrorToHost(OperationTableEventArgs eventArgs)
        {
            OnReceiveErrorDB?.Invoke(this, eventArgs);
        }


        private void HandlerUpData(object sender, DataEventArgs eventArgs)
        {
            try
            {
                Parallel.ForEach(clients, (client) =>
                {
                    Task.Run(() =>
                    {
                        try
                        {
                            client.Value.OperContext.GetCallbackChannel<IServerCallback>().DataCallback(eventArgs);
                        }
                        catch (Exception error)
                        {
                            client.Value.OperContext.Channel.Abort();
                            var result = clients.TryRemove(client.Value.ID, out ClientService removedClient);
                            if (result)
                            {
                                SendErrorOfClient(Errors.EnumServerError.ClientAbsent, $"Name = {removedClient.Name}, ID = {removedClient.ID}");
                                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Update, error.Message));
                            }
                        }
                    });

                });
            }
            catch (Exception excp)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Update, excp.Message + $" {excp.InnerException?.Message} "));
            }
        }


        private void HandlerAddRange(object sender, DataEventArgs eventArgs)
        {
            try
            {
                Parallel.ForEach(clients, (client) =>
                {
                    Task.Run(() =>
                    {
                        try
                        {
                            client.Value.OperContext.GetCallbackChannel<IServerCallback>().RangeCallBack(eventArgs);
                        }
                        catch (Exception error)
                        {
                            client.Value.OperContext.Channel.Abort();
                            var result = clients.TryRemove(client.Value.ID, out ClientService removedClient);
                            if (result)
                            {
                                SendErrorOfClient(Errors.EnumServerError.ClientAbsent, $"Name = {removedClient.Name}, ID = {removedClient.ID}");
                                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Update, error.Message));
                            }

                        }
                    });
                });
            }
            catch (Exception excp)
            {
                throw new InheritorsException.ExceptionLocalDB(0, excp.Message);
            }
        }


        private void HandlerUpRecord(object sender, RecordEventArgs eventArgs)
        {
            try
            {
                Parallel.ForEach(clients, (client) =>
                {
                    Task.Run(() =>
                    {
                        try
                        {
                            client.Value.OperContext.GetCallbackChannel<IServerCallback>().RecordCallBack(eventArgs);
                        }
                        catch (Exception error)
                        {
                            client.Value.OperContext.Channel.Abort();
                            var result = clients.TryRemove(client.Value.ID, out ClientService removedClient);
                            if (result)
                            {
                                SendErrorOfClient(Errors.EnumServerError.ClientAbsent, $"Name = {removedClient.Name}, ID = {removedClient.ID}");
                                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Update, error.Message));
                            }
                        }
                    });
                });

            }
            catch (Exception excp)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Update, excp.Message + $" {excp.InnerException?.Message} "));
            }
        }

        private void HandlerMessToHostOperation(object sender, string e)
        {
            OnMessToHost?.Invoke(sender, e);
        }
    }
}
