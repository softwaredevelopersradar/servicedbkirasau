﻿using System;
using System.Collections.Generic;
using ClientDataBase.ServiceDB;
using System.Text.RegularExpressions;
using KirasaUModelsDBLib;
using InheritorsEventArgs;
using DataEventArgs = InheritorsEventArgs.DataEventArgs;
using OperationTableEventArgs = InheritorsEventArgs.OperationTableEventArgs;
using System.Threading.Tasks;

namespace ClientDataBase
{
    public partial class ClientDB : IServiceDBCallback
    {
        private int ID;
        private string name = "";
        private string endpointAddress = "net.tcp://127.0.0.1:8302/";
        private ServiceDBClient ClientServiceDB;

        /// <summary>
        /// словарь, который хранит NameTable - имя таблицы, 
        /// ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей) 
        /// </summary>
        public readonly Dictionary<NameTable, IClassTables> Tables = new Dictionary<NameTable, IClassTables>
        {
            {NameTable.TableSectorsRecon, new ClassTable<TableSectorsRecon>() },
            { NameTable.TableFreqForbidden, new ClassTable<TableFreqForbidden>() },
            { NameTable.TableFreqRangesRecon, new ClassTable<TableFreqRangesRecon>() },
            { NameTable.TableRes, new ClassTable<TableRes>() },
            { NameTable.GlobalProperties, new ClassTable<GlobalProperties>() },
            { NameTable.TableResPattern, new ClassTable<TableResPattern>() },
            { NameTable.TableResArchive, new ClassTable<TableResArchive>() },
            { NameTable.TableTrackArchive, new ClassTable<TableTrackArchive>() },
            { NameTable.TableRemotePoints, new ClassTable<TableRemotePoints>() },
            { NameTable.TableResDistribution, new ClassTable<TableResDistribution>() },
            { NameTable.TableTrackDistribution, new ClassTable<TableTrackDistribution>() }
        };

        #region Events
        public event EventHandler<ClientEventArgs> OnConnect;
        public event EventHandler<ClientEventArgs> OnDisconnect;
        public event EventHandler<DataEventArgs> OnUpData;
        public event EventHandler<OperationTableEventArgs> OnErrorDataBase;
        #endregion

        private bool ValidEndPoint(string endpointAddress)
        {
            string ValidEndpointRegex = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";
            Regex r = new Regex(ValidEndpointRegex, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(endpointAddress);
            return m.Success;
        }

        public async void DataCallback(DataEventArgs lData)
        {
            try
            {
                await Task.Run(() =>
                {
                    OnUpData?.Invoke(this, lData);
                    (Tables[lData.Name] as IClickUpData).ClickUpTable(lData.AbstractData);
                }).ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase?.Invoke(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }

        public async void ErrorCallback(OperationTableEventArgs eventArgs)
        {
            await Task.Run(() => OnErrorDataBase?.Invoke(this, eventArgs)).ConfigureAwait(false);
        }

        public void Abort()
        {
            ClientServiceDB.Abort();
            ((IDisposable)ClientServiceDB).Dispose();
            ClientServiceDB = null;

            foreach (ClassTable table in Tables.Values)
                table.Dispose();
            OnDisconnect?.Invoke(null, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect));
        }

        public async void DataCallback(ServiceDB.DataEventArgs lData)
        {
            try
            {
                await Task.Run(() =>
                {
                    OnUpData?.Invoke(this, new DataEventArgs(lData.Name, lData.AbstractData));
                    (Tables[lData.Name] as IClickUpData).ClickUpTable(lData.AbstractData);
                }).ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase?.Invoke(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }

        public async void ErrorCallback(ServiceDB.OperationTableEventArgs error)
        {
            await Task.Run(() => OnErrorDataBase?.Invoke(this, new OperationTableEventArgs(error.Operation, error.TypeError)))
                .ConfigureAwait(false);
        }

        public async void RecordCallBack(ServiceDB.RecordEventArgs recordEventArgs)
        {
            await Task.Run(() => (Tables[recordEventArgs.Name] as IClickUpRecord)
            .ClickUpRecord(new InheritorsEventArgs.RecordEventArgs(recordEventArgs.Name, recordEventArgs.AbstractRecord, recordEventArgs.NameAction)))
            .ConfigureAwait(false);
        }

        public async void RangeCallBack(ServiceDB.DataEventArgs lData)
        {
            try
            {
                await Task.Run(() => (Tables[lData.Name] as IClickUpAddRange).ClickUpAddRange(lData.AbstractData))
                    .ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase?.Invoke(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }
    }
}
