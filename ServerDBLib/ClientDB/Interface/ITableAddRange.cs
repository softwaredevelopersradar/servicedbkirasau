﻿using InheritorsEventArgs;
using System;

namespace ClientDataBase
{
    public interface ITableAddRange<T> where T : KirasaUModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnAddRange;
    }
}
