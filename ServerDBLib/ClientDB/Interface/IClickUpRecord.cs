﻿using InheritorsEventArgs;

namespace ClientDataBase
{
    internal interface IClickUpRecord
    {
        void ClickUpRecord(RecordEventArgs eventArgs);
    }
}
