﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientDataBase
{
    internal interface IClickUpData
    {
        void ClickUpTable(KirasaUModelsDBLib.ClassDataCommon dataCommon);
    }
}
