﻿using InheritorsEventArgs;
using System;

namespace ClientDataBase
{
    public interface ITableUpdate<T> where T : KirasaUModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnUpTable;
    }
}
