﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using ClientDataBase;
using KirasaUModelsDBLib;
using InheritorsEventArgs;
using System.Threading;
using System.Threading.Tasks;

namespace TestClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ClientDB clientDB;
        string endPoint = "127.0.0.1:8302";

        public MainWindow()
        {
            InitializeComponent();
            this.Name = "TestClient";

        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }


        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;
        }
        

        async void HandlerConnect(object obj, EventArgs eventArgs)
        {
            btnCon.Content = "Disconnect";

            dynamic table = await clientDB.Tables[NameTable.TableSectorsRecon].LoadAsync<KirasaUModelsDBLib.TableSectorsRecon>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableSectorsRecon).ToString()} count records - {table.Count} \n");
            });


            table = await clientDB.Tables[NameTable.TableFreqForbidden].LoadAsync<KirasaUModelsDBLib.TableFreqForbidden>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableFreqForbidden).ToString()} count records - {table.Count} \n");
            });


            table = await clientDB.Tables[NameTable.TableFreqRangesRecon].LoadAsync<KirasaUModelsDBLib.TableFreqRangesRecon>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableFreqRangesRecon).ToString()} count records - {table.Count} \n");
            });


            table = await clientDB.Tables[NameTable.TableRes].LoadAsync<KirasaUModelsDBLib.TableRes>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableRes).ToString()} count records - {table.Count} \n");
            });


            table = await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<KirasaUModelsDBLib.GlobalProperties>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.GlobalProperties).ToString()} count records - {table.Count} \n");
            });
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            btnCon.Content = "Connect";
            if (eventArgs.GetMessage != "")
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(eventArgs.GetMessage);
            }
            clientDB = null;
        }

        async void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    tbMessage.Foreground = Brushes.Black;

                    tbMessage.AppendText($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");

                    foreach (var rec in eventArgs.AbstractData.ListRecords)
                    {
                        tbMessage.AppendText($"Id: {rec.Id}\n");
                    }
                });
            });

        }

        void HandlerErrorDataBase(object obj, OperationTableEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(eventArgs.GetMessage);
            });
        }


        int index = 1;
        private async void ButAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                            AngleMax = Convert.ToInt16(rand.Next(0, 360))
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableRes:
                        record = new TableRes
                        {
                            Id = index,
                            FrequencyKHz = rand.NextDouble(),
                            Bearing = rand.Next(0, 360),
                            //StandardDeviation = 2,
                            Pulse = new ParamDP()
                            {
                                Duration = rand.Next(0, 500),
                                Period = rand.Next(0, 500)
                            },
                            Series = new ParamDP()
                            {
                                Duration = rand.Next(0, 500),
                                Period = rand.Next(0, 500)
                            },
                            Note = DateTime.Now.ToString(),
                            AntennaMaster = 2,
                            AntennaSlave = 4
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            Id = 1,
                            Location = new Location() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            //Calibration = new Calibration() { Type = CalibrationTypes.Generator },
                            //RadioIntelegence = new RadioIntelegence()
                            //{
                            //    CourseAngle = 0,
                            //    SynchronizeType = SynchronizeTypes.GPS
                            //}
                        };
                        break;


                }
                index++;
                if (record != null)
                    await clientDB?.Tables[(NameTable)Tables.SelectedItem].AddAsync(record);
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private async void ButDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableRes:
                        record = new TableRes
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            Id = nufOfRec
                        };
                        break;
                    default:
                        break;


                }
                if (record != null)
                    await clientDB?.Tables[(NameTable)Tables.SelectedItem].DeleteAsync(record);
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private async void ButRemove_Click(object sender, RoutedEventArgs e)
        {
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            Random rand = new Random();
            //object record = null;
            dynamic list = new object();
            switch (Tables.SelectedItem)
            {
                case NameTable.TableSectorsRecon:
                    list = new List<TableSectorsRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableSectorsRecon
                        {
                            Id = i
                        };
                        list.Add(record);
                    }

                    break;
                case NameTable.TableFreqForbidden:
                    list = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableFreqForbidden
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableFreqRangesRecon:
                    list = new List<TableFreqRangesRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableRes:
                    list = new List<TableRes>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableRes
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                default:
                    break;


            }
            if (list != null)
                await clientDB?.Tables[(NameTable)Tables.SelectedItem].RemoveRangeAsync(list);
        }


        private async void SendRange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Random rand = new Random();
                //object record = null;
                switch (Tables.SelectedItem)
                {
                    case NameTable.TableSectorsRecon:
                        List<TableSectorsRecon> listForSend = new List<TableSectorsRecon>();
                        for (int i = 0; i < 8; i++)
                        {
                            TableSectorsRecon record = new TableSectorsRecon
                            {
                                AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                                AngleMax = Convert.ToInt16(rand.Next(0, 360))
                            };
                            listForSend.Add(record);
                        }
                        await clientDB.Tables[NameTable.TableSectorsRecon].AddRangeAsync(listForSend);
                        break;
                    case NameTable.TableFreqForbidden:
                        List<TableFreqForbidden> listForSend2 = new List<TableFreqForbidden>();
                        for (int i = 0; i < 8; i++)
                        {
                            TableFreqForbidden record = new TableFreqForbidden
                            {
                                FreqMaxKHz = rand.Next(150000, 250000),
                                FreqMinKHz = rand.Next(30000, 50000),
                            };
                            listForSend2.Add(record);
                        }
                        await clientDB.Tables[NameTable.TableFreqForbidden].AddRangeAsync(listForSend2);
                        break;
                    case NameTable.TableFreqRangesRecon:
                        List<TableFreqRangesRecon> listForSend3 = new List<TableFreqRangesRecon>();
                        for (int i = 0; i < 8; i++)
                        {
                            TableFreqRangesRecon record = new TableFreqRangesRecon
                            {
                                //Id = rand.Next(1, 1000),
                                FreqMaxKHz = rand.Next(150000, 250000),
                                FreqMinKHz = rand.Next(30000, 50000),
                            };
                            listForSend3.Add(record);
                        }
                        await clientDB.Tables[NameTable.TableFreqRangesRecon].AddRangeAsync(listForSend3);
                        break;
                    case NameTable.TableRes:
                        List<TableRes> listForSend4 = new List<TableRes>();
                        for (int i = 0; i < 8; i++)
                        {
                            TableRes record = new TableRes
                            {
                                Id = index,
                                FrequencyKHz = rand.NextDouble(),
                                Bearing = rand.Next(0, 360),
                                //StandardDeviation = 3,
                                Pulse = new ParamDP()
                                {
                                    Duration = rand.Next(0, 500),
                                    Period = rand.Next(0, 500)
                                },
                                Series = new ParamDP()
                                {
                                    Duration = rand.Next(0, 500),
                                    Period = rand.Next(0, 500)
                                },
                                Note = DateTime.Now.ToString()

                            };
                            index++;
                            listForSend4.Add(record);
                        }
                        await clientDB.Tables[NameTable.TableRes].AddRangeAsync(listForSend4);
                        break;
                }

            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private async void ButClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await clientDB?.Tables[(NameTable)Tables.SelectedItem].ClearAsync();
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private async void ButLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tbMessage.Foreground = Brushes.Black;


                List<AbstractCommonTable> table = new List<AbstractCommonTable>();
                NameTable nameTable = (NameTable)Tables.SelectedItem;

                table = await clientDB.Tables[nameTable].LoadAsync<AbstractCommonTable>();
                tbMessage.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {table.Count} \n");

            }
            catch (ClientDataBase.ExceptionClient exeptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exeptClient.Message);
            }
            catch (ClientDataBase.ExceptionDatabase excpetService)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(excpetService.Message);
            }
        }

        private async void butChange_Click(object sender, RoutedEventArgs e)
        {
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            Random rand = new Random();
            object record = null;
            switch (Tables.SelectedItem)
            {

                case NameTable.TableSectorsRecon:
                    record = new TableSectorsRecon
                    {
                        Id = nufOfRec,
                        AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                        AngleMax = Convert.ToInt16(rand.Next(0, 360))
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    record = new TableFreqForbidden
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000),
                    };
                    break;
                case NameTable.TableFreqRangesRecon:
                    record = new TableFreqRangesRecon
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000),
                    };
                    break;
                case NameTable.TableRes:
                    record = new TableRes
                    {
                        Id = nufOfRec,
                        FrequencyKHz = rand.NextDouble(),
                        Bearing = rand.Next(0, 360),
                        //StandardDeviation = 4,
                        Pulse = new ParamDP()
                        {
                            Duration = rand.Next(0, 500),
                            Period = rand.Next(0, 500)
                        },
                        Series = new ParamDP()
                        {
                            Duration = rand.Next(0, 500),
                            Period = rand.Next(0, 500)
                        },
                        Note = DateTime.Now.ToString()
                    };
                    break;
                case NameTable.GlobalProperties:
                    record = new GlobalProperties
                    {
                        Id = 1,
                        Location = new Location() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                        //Calibration = new Calibration() { Type = CalibrationTypes.Load },
                        //RadioIntelegence = new RadioIntelegence()
                        //{
                        //    CourseAngle = 0,
                        //    SynchronizeType = SynchronizeTypes.GPS
                        //}
                    };
                    break;


            }
            if (record != null)
                await clientDB?.Tables[(NameTable)Tables.SelectedItem].ChangeAsync(record);
        }

        private void btnCon_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.DisconnectAsync();
                else
                {
                    clientDB = new ClientDB(this.Name, endPoint);
                    InitClientDB();
                    clientDB.ConnectAsync();
                }
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }
    }
}
