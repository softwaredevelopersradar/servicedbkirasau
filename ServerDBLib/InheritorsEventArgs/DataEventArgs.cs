﻿using System;
using KirasaUModelsDBLib;
using System.Runtime.Serialization;

namespace InheritorsEventArgs
{
    [DataContract]
    [KnownType(typeof(ClassDataCommon))]
    [KnownType(typeof(NameTable))]
    public class DataEventArgs : EventArgs
    {
        [DataMember]
        public ClassDataCommon AbstractData { get; protected set; }

        [DataMember]
        public NameTable Name { get; protected set; }

        public DataEventArgs(NameTable nameTable, ClassDataCommon data)
        {
            Name = nameTable;
            AbstractData = data;
        }
    }
}
