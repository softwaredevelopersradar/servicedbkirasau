﻿using System.Runtime.Serialization;

namespace ServerHostKiraU
{
    [DataContract]
    public class PropertyHost
    {
        [DataMember]
        public string IpAddress { get; set; }

        [DataMember]
        public int PortHttp { get; set; }

        [DataMember]
        public int PortTcp { get; set; }

        public PropertyHost(string ipAddress, int portHttp, int portTcp)
        {
            IpAddress = ipAddress;
            PortHttp = portHttp;
            PortTcp = portTcp;
        }
    }
}
