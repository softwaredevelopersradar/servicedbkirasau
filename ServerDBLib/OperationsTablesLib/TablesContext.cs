﻿using System;
using KirasaUModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationsTablesLib
{
    public class TablesContext : DbContext
    {
        #region DbSet
        public DbSet<TableSectorsRecon> TSectorsRecon { get; set; }
        public DbSet<GlobalProperties> TGlobalProperties { get; set; }
        public DbSet<TableFreqForbidden> TFreqForbidden { get; set; }
        public DbSet<TableFreqRangesRecon> TFreqRangesRecon { get; set; }
        public DbSet<TableResArchive> TResArchive { get; set; }
        public DbSet<TableTrackArchive> TTrackArchive { get; set; }
        public DbSet<TableResPattern> TResPattern { get; set; }
        public DbSet<TableRemotePoints> TRemotePoints { get; set; }
        #endregion

        public DbSet<T> GetTable<T>(NameTable name) where T : class
        {
            switch (name)
            {
                case NameTable.TableFreqForbidden:
                    return TFreqForbidden as DbSet<T>;
                case NameTable.TableFreqRangesRecon:
                    return TFreqRangesRecon as DbSet<T>;
                case NameTable.TableSectorsRecon:
                    return TSectorsRecon as DbSet<T>;
                case NameTable.GlobalProperties:
                    return TGlobalProperties as DbSet<T>;
                case NameTable.TableResArchive:
                    return TResArchive as DbSet<T>;
                case NameTable.TableTrackArchive:
                    return TTrackArchive as DbSet<T>;
                case NameTable.TableResPattern:
                    return TResPattern as DbSet<T>;
                case NameTable.TableRemotePoints:
                    return TRemotePoints as DbSet<T>;
                default:
                    return null;
            }
        }

        public TablesContext()
        {
            try
            {
               SQLitePCL.Batteries.Init();
            }
            catch (Exception ex)
            {
                Console.Write($"Error: {ex.Message}");
            }

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging(true);
            var temp = System.IO.Directory.GetCurrentDirectory();
            var location = System.Reflection.Assembly.GetExecutingAssembly();
            string path = "Filename=" + System.IO.Directory.GetCurrentDirectory() + "\\KirasaU_DB.db";
            optionsBuilder.UseSqlite(path);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Coord>().Property(rec => rec.Altitude).HasDefaultValue(-1);
            modelBuilder.Entity<Coord>().Property(rec => rec.Latitude).HasDefaultValue(-1);
            modelBuilder.Entity<Coord>().Property(rec => rec.Longitude).HasDefaultValue(-1);

            modelBuilder.Entity<TableResPattern>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResArchive>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableTrackArchive>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResArchive>().HasMany(rec => rec.Track).WithOne().OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableRemotePoints>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableRemotePoints>().OwnsOne(t => t.Coordinates);

            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.CourseAngle).HasDefaultValue(0);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.SynchronizeType).HasDefaultValue(SynchronizeTypes.Internal);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.DurationMax).HasDefaultValue(2000);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.PeriodMax).HasDefaultValue(10000);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.NarrowBand).HasDefaultValue(5);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.DurationMin).HasDefaultValue(2000);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.PeriodMin).HasDefaultValue(10000);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.NumberOfReviews).HasDefaultValue(10);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.DeviceAnalysisType).HasDefaultValue(DeviceAnalysisTypes.Both);

            //modelBuilder.Entity<Calibration>().Property(rec => rec.Type).HasDefaultValue(CalibrationTypes.Generator);

            //modelBuilder.Entity<IQData>().Property(rec => rec.TimeRecord).HasDefaultValue(8);
            //modelBuilder.Entity<IQData>().Property(rec => rec.CaptureMode).HasDefaultValue(CaptureMode.Auto);
            //modelBuilder.Entity<IQData>().Property(rec => rec.InputBuffer).HasDefaultValue(BufferSizes.x32);
            //modelBuilder.Entity<IQData>().Property(rec => rec.OutputBuffer).HasDefaultValue(BufferSizes.x32);
            //modelBuilder.Entity<IQData>().Property(rec => rec.WaitForStrobe).HasDefaultValue(true);
            //modelBuilder.Entity<IQData>().Property(rec => rec.Recorder).HasDefaultValue(Recorder.Master);
            modelBuilder.Entity<Location>().Property(rec => rec.Altitude).HasDefaultValue(-1);
            modelBuilder.Entity<Location>().Property(rec => rec.Latitude).HasDefaultValue(-1);
            modelBuilder.Entity<Location>().Property(rec => rec.Longitude).HasDefaultValue(-1);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Location);
            //modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.RadioIntelegence);
            //modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Calibration);
            //modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.IQData);

            modelBuilder.Entity<GlobalProperties>(b =>
            {
                b.HasData(new
                {
                    Id = 1
                });

                b.OwnsOne(e => e.Location).HasData(new
                {
                    GlobalPropertiesId = 1,
                    Altitude = (float)-1,
                    Latitude = (double)-1,
                    Longitude = (double)-1,
                    CourseAngle = (float)0,
                    CompassAngle = (float)0

                });

                //b.OwnsOne(e => e.Calibration).HasData(new
                //{
                //    GlobalPropertiesId = 1,
                //    Type = CalibrationTypes.Generator
                //});

                //b.OwnsOne(e => e.Location).HasData(new
                //{
                //    GlobalPropertiesId = 1,
                //    Altitude = (float)-1,
                //    Latitude = (float)-1,
                //    Longitude = (float)-1
                //});

                //b.OwnsOne(e => e.RadioIntelegence).HasData(new
                //{
                //    GlobalPropertiesId = 1,
                //    CourseAngle = (float)0,
                //    SynchronizeType = SynchronizeTypes.Internal,
                //    NarrowBand = (float)5,
                //    DurationMin = (float)2000,
                //    DurationMax = (float)2000,
                //    PeriodMin = (float)10000,
                //    PeriodMax = (float)10000,
                //    NumberOfReviews = (byte)10,
                //    DeviceAnalysisType = DeviceAnalysisTypes.Both

                //});

                //b.OwnsOne(e => e.IQData).HasData(new
                //{
                //    GlobalPropertiesId = 1,
                //    TimeRecord = (float)8,
                //    PathFolderNbFiles = (string)"",
                //    WaitForStrobe = true,
                //    CaptureMode = CaptureMode.Auto,
                //    InputBuffer = BufferSizes.x32,
                //    OutputBuffer = BufferSizes.x32,
                //    Recorder = Recorder.Master
                //});


            });
        }
    }
}
