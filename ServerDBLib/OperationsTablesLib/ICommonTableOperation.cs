﻿using System.ServiceModel;
using KirasaUModelsDBLib;
using InheritorsEventArgs;
using InheritorsException;

namespace OperationsTablesLib
{
    [ServiceContract(CallbackContract = typeof(ICommonCallback))]
    #region KnownType
    [ServiceKnownType(typeof(ClassDataCommon))]
    [ServiceKnownType(typeof(AbstractCommonTable))]
    [ServiceKnownType(typeof(TableFreqForbidden))]
    [ServiceKnownType(typeof(TableFreqRangesRecon))]
    [ServiceKnownType(typeof(TableFreqRanges))]
    [ServiceKnownType(typeof(TableSectorsRecon))]
    [ServiceKnownType(typeof(GlobalProperties))]
    [ServiceKnownType(typeof(TableRes))]
    [ServiceKnownType(typeof(TableResArchive))]
    [ServiceKnownType(typeof(TableResDistribution))]
    [ServiceKnownType(typeof(TableResPattern))]
    [ServiceKnownType(typeof(TableTrack))]
    [ServiceKnownType(typeof(TableTrackArchive))]
    [ServiceKnownType(typeof(TableTrackDistribution))]
    [ServiceKnownType(typeof(TableRemotePoints))]



    [ServiceKnownType(typeof(NameTable))]
    [ServiceKnownType(typeof(Languages))]
    [ServiceKnownType(typeof(NameChangeOperation))]
    [ServiceKnownType(typeof(NameTableOperation))]
    [ServiceKnownType(typeof(TypeUAVRes))]
    [ServiceKnownType(typeof(CalibrationTypes))]
    [ServiceKnownType(typeof(SynchronizeTypes))]
    [ServiceKnownType(typeof(DeviceAnalysisTypes))]
    [ServiceKnownType(typeof(CaptureMode))]
    [ServiceKnownType(typeof(BufferSizes))]
    [ServiceKnownType(typeof(Recorder))]
    [ServiceKnownType(typeof(ModulationType))]
    [ServiceKnownType(typeof(ReceiveSignal))]
    [ServiceKnownType(typeof(WorkingMode))]
    [ServiceKnownType(typeof(ClassTarget))]
    [ServiceKnownType(typeof(CoordinateSystem))]
    

    [ServiceKnownType(typeof(ExceptionWCF))]
    #endregion
    public interface ICommonTableOperation
    {
        [OperationContract(IsOneWay = true)]
        void ChangeRecord(NameTable nameTable, NameChangeOperation nameAction, AbstractCommonTable record, int IdClient);

        [OperationContract(IsOneWay = true)]
        void AddRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        void RemoveRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        void ClearTable(NameTable nameTable, int IdClient);

        [OperationContract]
        [FaultContract(typeof(ExceptionWCF))]
        [ServiceKnownType(typeof(ExceptionWCF))]
        ///<summary>
        ///Загрузка данных для выбранной обстановки
        ///</summary>
        ClassDataCommon LoadData(NameTable nameTable, int IdClient);
    }

    public interface ICommonCallback
    {
        /// <summary>
        /// Получены данные из БД
        /// </summary>
        /// <param name="lData">Данные из таблицы</param>
        [OperationContract(IsOneWay = true)]
        void DataCallback(DataEventArgs dataEventArgs);

        [OperationContract(IsOneWay = true)]
        void RecordCallBack(RecordEventArgs recordEventArgs);

        [OperationContract(IsOneWay = true)]
        void RangeCallBack(DataEventArgs dataEventArgs);
    }
}
