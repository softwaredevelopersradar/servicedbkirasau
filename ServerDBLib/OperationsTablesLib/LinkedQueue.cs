﻿using System;
using System.Collections.Generic;

namespace OperationsTablesLib
{
    public class LinkedQueue<T> : LinkedList<T>
    {

        public void Enqueue(T item)
        {
            base.AddLast(item);
        }

        public T Dequeue()
        {
            if (this.First == null)
                throw new InvalidOperationException("...");

            var item = First.Value;
            RemoveFirst();

            return item;
        }

    }
}
