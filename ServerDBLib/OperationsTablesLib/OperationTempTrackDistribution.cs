﻿using System;
using System.Collections.Generic;
using System.Linq;
using KirasaUModelsDBLib;
using InheritorsEventArgs;

namespace OperationsTablesLib
{
    public class OperationTempTrackDistribution : Operation, ITableAction
    {
        internal static LinkedQueue<KeyValuePair<int, TableTrackDistribution>> TempTable = new LinkedQueue<KeyValuePair<int, TableTrackDistribution>>();
        object locker = new object();

        public OperationTempTrackDistribution() : base()
        {
            IsTemp = true;
            Name = NameTable.TableTrackDistribution;
        }

        public void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                var findUAV = OperationTempResDistribution.TempTable.Values.Where(t => t.Id == (record as TableTrackDistribution).TableResId).ToList().Count;
                if (findUAV == 0)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchResIsAbsent);
                }

                if (TempTable.Where(c => c.Key == (int)record.GetKey().FirstOrDefault()).ToList().Count != 0)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordExist);
                }

                TempTable.Enqueue(new KeyValuePair<int, TableTrackDistribution>((int)record.GetKey().FirstOrDefault(), record as TableTrackDistribution));

                //if (IsLimited)
                //{
                //    if (TempTable.Count > TableLimit)
                //        TempTable.Dequeue();
                //}

                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    int key = (int)rec.GetKey().FirstOrDefault();
                    var foundedRecord = TempTable.Where(c => c.Key == key).FirstOrDefault();
                    if (!foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                    {
                        TempTable.Find(foundedRecord).Value.Value.Update(rec as TableTrackDistribution);
                    }
                    else
                    {
                        TempTable.Enqueue(new KeyValuePair<int, TableTrackDistribution>(key, rec as TableTrackDistribution));

                        //if (TempTable.Count > 1000)
                        //    TempTable.Dequeue();
                    }
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    int key = (int)rec.GetKey().FirstOrDefault();
                    var foundedRecord = TempTable.Where(c => c.Key == key).FirstOrDefault();
                    if (!foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                    {
                        TempTable.Remove(foundedRecord);
                    }
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                int key = (int)record.GetKey().FirstOrDefault();

                var foundedRecord = TempTable.Where(c => c.Key == key).FirstOrDefault();
                if (foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                //int index = TempTable.Select((item, inx) => new { item, inx })
                //  .First(x => x.item.Equals(foundedRecord)).inx;
                TempTable.Find(foundedRecord).Value.Value.Update(record as TableTrackDistribution); //TODO: check
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void Clear(int idClient)
        {
            try
            {
                TempTable.Clear();
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                int key = (int)record.GetKey().FirstOrDefault();
                var foundedRecord = TempTable.Where(c => c.Key == key).FirstOrDefault();
                if (foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }

                TempTable.Remove(foundedRecord);
                UpRecord(idClient, record, NameChangeOperation.Delete);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void UpDate(int idClient)
        {
            try
            {
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(TempTable.ToList().Select(kv => kv.Value).ToList())));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void UpRecord(int idClient, AbstractCommonTable record, NameChangeOperation action)
        {
            try
            {
                SendUpRecord(this, new RecordEventArgs(Name, record, action));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public ClassDataCommon Load(int idClient)
        {
            try
            {
                var temp = TempTable.ToList().Select(kv => kv.Value).ToList();
                return ClassDataCommon.ConvertToListAbstractCommonTable(temp);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
