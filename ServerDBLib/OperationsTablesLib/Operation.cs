﻿using System;
using KirasaUModelsDBLib;
using InheritorsEventArgs;
using System.Threading.Tasks;

namespace OperationsTablesLib
{
    public class Operation
    {
        protected static TablesContext DataBase;

        protected NameTable Name;

        public bool IsTemp { get; protected set; }

        public Operation()
        {
            IsTemp = false;
            if (DataBase != null) return;
            DataBase = new TablesContext();
        }

        public static event EventHandler<DataEventArgs> OnReceiveData;

        public static event EventHandler<DataEventArgs> OnAddRange;

        public static event EventHandler<RecordEventArgs> OnReceiveRecord;

        public static event EventHandler<string> OnSendMessToHost;


        internal void SendUpData(object obj, DataEventArgs data)
        {
            Task.Run(() => OnReceiveData?.Invoke(obj, data));
        }

        internal void SendRange(object obj, DataEventArgs data)
        {
            Task.Run(() => OnAddRange?.Invoke(obj, data));
        }

        internal void SendUpRecord(object obj, RecordEventArgs eventArgs)
        {
            Task.Run(() => OnReceiveRecord?.Invoke(obj, eventArgs));
        }

        internal void SendMessToHost(object obj, string mess)
        {
            Task.Run(() => OnSendMessToHost?.Invoke(obj, mess));
        }
    }
}
