﻿using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Таблица Точки маршрута ИРИ Архив
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableTrack))]
    [InfoTable(NameTable.TableTrackArchive)]
    public class TableTrackArchive : TableTrack
    {
    }
}
