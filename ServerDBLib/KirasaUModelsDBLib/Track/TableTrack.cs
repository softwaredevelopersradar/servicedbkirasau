﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Таблица Точки маршрута
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableTrackArchive))]
    [KnownType(typeof(TableTrackDistribution))]
    public class TableTrack : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public float Tau1 { get; set; }

        [DataMember]
        public float Tau2 { get; set; }

        [DataMember]
        public float Tau3 { get; set; }

        [DataMember]
        public float Tau4 { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public int TableResId { get; set; }
        


        public TableTrack() { }
        public TableTrack(int tableResId, DateTime time, float tau1, float tau2, float tau3, float tau4, Coord coord, int id = 0)
        {
            Id = id;
            Time = time;
            Tau1 = tau1;
            Tau2 = tau2;
            Tau3 = tau3;
            Tau4 = tau4;
            Coordinates = coord.Clone();
            TableResId = tableResId;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableTrack)record;

            Time = newRecord.Time;
            Tau1 = newRecord.Tau1;
            Tau2 = newRecord.Tau2;
            Tau3 = newRecord.Tau3;
            Tau4 = newRecord.Tau4;
            Coordinates.Update(newRecord.Coordinates);
            TableResId = newRecord.TableResId;
        }

        public TableTrack Clone() => new TableTrack(TableResId, Time, Tau1, Tau2, Tau3, Tau4, Coordinates, Id);


        public TableTrackArchive ToTrackArchive()
        {
            return new TableTrackArchive()
            {
                Id = Id,
                Time = Time,
                Tau1 = Tau1,
                Tau2 = Tau2,
                Tau3 = Tau3,
                Tau4 = Tau4,
                Coordinates = Coordinates.Clone(),
                TableResId = TableResId
            };
        }

        public TableTrackDistribution ToTrackDistribution()
        {
            return new TableTrackDistribution()
            {
                Id = Id,
                Time = Time,
                Tau1 = Tau1,
                Tau2 = Tau2,
                Tau3 = Tau3,
                Tau4 = Tau4,
                Coordinates = Coordinates.Clone(),
                TableResId = TableResId
            };
        }
    }
}
