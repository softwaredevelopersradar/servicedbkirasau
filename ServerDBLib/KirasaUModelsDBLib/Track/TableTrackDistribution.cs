﻿using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Таблица Точки маршрута ИРИ ЦР
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableTrack))]
    [InfoTable(NameTable.TableTrackDistribution)]
    public class TableTrackDistribution : TableTrack
    {

    }
}
