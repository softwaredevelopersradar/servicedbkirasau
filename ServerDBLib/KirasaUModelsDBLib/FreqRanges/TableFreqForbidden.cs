﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Запрещенные частоты (ЗЧ)
    /// </summary>
    [DataContract]
    [CategoryOrder("Диапазон", 2)]
    [CategoryOrder("Импульс", 1)]
    [KnownType(typeof(TableFreqRanges))]
    [InfoTable(NameTable.TableFreqForbidden)]

    public class TableFreqForbidden : TableFreqRanges
    {
        [DataMember]
        [Category("Импульс")]
        //[Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse { get; set; } = new ParamDP();

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableFreqForbidden)record;
            FreqMaxKHz = newRecord.FreqMaxKHz;
            FreqMinKHz = newRecord.FreqMinKHz;
            Note = newRecord.Note;
            IsActive = newRecord.IsActive;
            Pulse.Duration = newRecord.Pulse.Duration;
            Pulse.Period = newRecord.Pulse.Period;
        }

        public new TableFreqForbidden Clone()
        {
            return new TableFreqForbidden
            {
                Id = this.Id,
                FreqMinKHz = this.FreqMinKHz,
                FreqMaxKHz = this.FreqMaxKHz,
                Note = this.Note,
                IsActive = this.IsActive,
                Pulse = this.Pulse 
            };
        }
    }
}
