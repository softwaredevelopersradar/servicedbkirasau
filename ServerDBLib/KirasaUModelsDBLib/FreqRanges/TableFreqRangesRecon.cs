﻿using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Диапазоны радиоразведки (ДРР)
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableFreqRanges))]
    [InfoTable(NameTable.TableFreqRangesRecon)]

    public class TableFreqRangesRecon : TableFreqRanges
    {
    }
}
