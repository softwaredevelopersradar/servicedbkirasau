﻿
namespace KirasaUModelsDBLib.Interfaces
{
    public interface IFixFreqRanges
    {
        int Id { get; set; }

        double FreqMinKHz { get; set; }

        double FreqMaxKHz { get; set; }
    }
}
