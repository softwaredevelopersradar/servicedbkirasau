﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using KirasaUModelsDBLib.Interfaces;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Диапазоны частот
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Диапазон", 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(TableFreqRangesRecon))]
    [KnownType(typeof(TableFreqForbidden))]

    public class TableFreqRanges : AbstractCommonTable, IFixFreqRanges
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("Диапазон")]
        [PropertyOrder(1)]
        [DisplayName(nameof(FreqMinKHz))]
        public double FreqMinKHz { get; set; } 

        [DataMember]
        [Category("Диапазон")]
        [PropertyOrder(2)]
        [DisplayName(nameof(FreqMaxKHz))]
        public double FreqMaxKHz { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public String Note { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }  

        public TableFreqRanges Clone()
        {
            return new TableFreqRanges
            {
                Id = this.Id,
                FreqMinKHz = this.FreqMinKHz,
                FreqMaxKHz = this.FreqMaxKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
        }

        public TableFreqRangesRecon ToRangesRecon()
        {
            TableFreqRangesRecon table = new TableFreqRangesRecon()
            {
                Id = this.Id,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
            return table;
        }

        public TableFreqForbidden ToFreqForbidden()
        {
            TableFreqForbidden table = new TableFreqForbidden()
            {
                Id = this.Id,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
            return table;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableFreqRanges)record;
            FreqMaxKHz = newRecord.FreqMaxKHz;
            FreqMinKHz = newRecord.FreqMinKHz;
            Note = newRecord.Note;
            IsActive = newRecord.IsActive;
        }
    }
}
