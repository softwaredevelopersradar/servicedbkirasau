﻿using System;

namespace KirasaUModelsDBLib
{
    public class InfoTableAttribute : Attribute
    {
        public NameTable Name { get; private set; }

        public InfoTableAttribute(NameTable name)
        {
            Name = name;
        }
    }
}
