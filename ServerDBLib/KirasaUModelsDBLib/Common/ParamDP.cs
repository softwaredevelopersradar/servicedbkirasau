﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    [DataContract]
    public class ParamDP
    {
        [DataMember]
        [DisplayName(nameof(Duration))]
        [NotifyParentProperty(true)]
        [Category(nameof(ParamDP))]
        public float Duration { get; set; }

        [DataMember]
        [DisplayName(nameof(Period))]
        [NotifyParentProperty(true)]
        [Category(nameof(ParamDP))]
        public float Period { get; set; }

        public ParamDP() { }

        public ParamDP(float duration, float period) 
        {
            Duration = duration;
            Period = period;
        }

        public ParamDP Clone() => new ParamDP(Duration, Period);

        public void Update(ParamDP record)
        {
            Duration = record.Duration;
            Period = record.Period;
        }
    }
}
