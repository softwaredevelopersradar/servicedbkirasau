﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    [DataContract]
    public enum NameChangeOperation : byte
    {
        [EnumMember]
        Add,
        [EnumMember]
        Delete,
        [EnumMember]
        Change
    }

    [DataContract]
    public enum NameTableOperation : byte
    {
        [EnumMember]
        Add,
        [EnumMember]
        AddRange,
        [EnumMember]
        RemoveRange,
        [EnumMember]
        Change,
        [EnumMember]
        Delete,
        [EnumMember]
        Clear,
        [EnumMember]
        Load,
        [EnumMember]
        Update,
        [EnumMember]
        None
    }

    [DataContract]
    public enum NameTable : byte
    {
        [EnumMember]
        [Description("ИРИ")]
        TableRes,

        [EnumMember]
        [Description("Диапазоны РР")]
        TableFreqRangesRecon, 

        [EnumMember]
        [Description("Запрещенные частоты")]
        TableFreqForbidden,

        [EnumMember]
        [Description("Сектора РР")]
        TableSectorsRecon,

        [EnumMember]
        [Description("Глобальные настройки")]
        GlobalProperties,

        [EnumMember]
        [Description("ИРИ ЦР")]
        TableResDistribution,

        [EnumMember]
        [Description("Точки траектории ИРИ ЦР")]
        TableTrackDistribution,

        [EnumMember]
        [Description("ИРИ Архив")]
        TableResArchive,

        [EnumMember]
        [Description("Точки траектории Архива")]
        TableTrackArchive,

        [EnumMember]
        [Description("Удаленные пункты приема")]
        TableRemotePoints,

        [EnumMember]
        [Description("Шаблоны ИРИ")]
        TableResPattern
    }

    public enum Languages : byte
    {
        [Description("Русский")]
        Rus,
        [Description("English")]
        Eng
    }


    public enum TypeUAVRes : byte
    {
        [Description("Unknown")]
        Unknown,
        [Description("Lightbridge")]
        Lightbridge,
        [Description("Ocusinc")]
        Ocusinc,
        [Description("Wi-Fi (M)")]
        WiFi,
        [Description("3G")]
        G3
    }


    [DataContract]
    public enum CalibrationTypes : byte
    {
        [EnumMember]
        Generator = 1,
        [EnumMember]
        Load = 2
    }

    [DataContract]
    public enum SynchronizeTypes : byte
    {
        [EnumMember]
        Internal = 1,
        [EnumMember]
        GPS = 2
    }

    [DataContract]
    public enum DeviceAnalysisTypes : byte
    {
        [EnumMember]
        Master,
        [EnumMember]
        Slave,
        [EnumMember]
        Both
    }

    [DataContract]
    public enum CaptureMode : byte
    {
        [EnumMember]
        Single,
        [EnumMember]
        Auto
    }


    [DataContract]
    public enum BufferSizes : byte
    {
        [EnumMember]
        x1,
        [EnumMember]
        x2,
        [EnumMember]
        x4,
        [EnumMember]
        x8,
        [EnumMember]
        x16,
        [EnumMember]
        x32,
        [EnumMember]
        x64,
        [EnumMember]
        x128,
        [EnumMember]
        x256
    }

    [DataContract]
    public enum Recorder : byte
    {
        [EnumMember]
        Master,
        [EnumMember]
        Slave
    }

    [DataContract]
    public enum ModulationType : byte
    {
        [EnumMember]
        AM,
        [EnumMember]
        FM,
        [EnumMember]
        UM,
        [EnumMember]
        LFM
    }

    [DataContract]
    public enum ReceiveSignal : byte
    {
        [EnumMember]
        NoAnswer,
        [EnumMember]
        NoSignal,
        [EnumMember]
        YesSignal,
        [EnumMember]
        BadSignal
    }

    [DataContract]
    public enum WorkingMode : byte
    {
        [EnumMember]
        AirLand,
        [EnumMember]
        AirAir,
        [EnumMember]
        LandLand
    }

    [DataContract]
    public enum ClassTarget : byte
    {
        [EnumMember]
        StaticObject,
        [EnumMember]
        UAV,
        [EnumMember]
        Aircraft,
        [EnumMember]
        Helicopter,
        [EnumMember]
        Radar,
        [EnumMember]
        SurvRadar, //обзорная РЛС
        [EnumMember]
        FCRadar, // РЛС Управления Огнём
        [EnumMember]
        WCRadar, //Метеостанция
        [EnumMember]
        LandingRadar
    }

    [DataContract]
    public enum CoordinateSystem : byte
    {
        [EnumMember]
        MGRS,
        [EnumMember]
        UTM,
        [EnumMember]
        NEH
    }

}
