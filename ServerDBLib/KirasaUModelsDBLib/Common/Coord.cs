﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Координаты
    /// </summary>
    [DataContract]
    public class Coord : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private double latitude = -1;
        private double longitude = -1;
        private float altitude = -1;

        #endregion

        [DataMember]
        [DisplayName(nameof(Latitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude == value)
                    return;
                latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        [DataMember]
        [DisplayName(nameof(Longitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude == value)
                    return;
                longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        [DataMember]
        [DisplayName(nameof(Altitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public float Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;
                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }


        public Coord() { }
        public Coord(double latitude, double longitude, float altitude)
        {
            Altitude = altitude;
            Latitude = latitude;
            Longitude = longitude;
        }

        public void Update(Coord record)
        {
            Altitude = record.Altitude;
            Latitude = record.Latitude;
            Longitude = record.Longitude;
        }

        public Coord Clone() => new Coord(Latitude, Longitude, Altitude);

        public override string ToString()
        {
            if ((Latitude < 0 && Longitude < 0 && Altitude < 0) || (Latitude < 0 || Longitude < 0))
                return " ";
            if (Altitude < 0)
                return $"{latitude} : {longitude} : — ";
            return $"{latitude} : {longitude} : {altitude}";
        }
    }
}
