﻿using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    [DataContract]
    //[KnownType(typeof(TableRes))]
    public abstract class AbstractCommonTable
    {
        [DataMember]
        public abstract int Id { get; set; }

        public object[] GetKey() => new object[] { Id };

        public abstract void Update(AbstractCommonTable record);
    }
}
