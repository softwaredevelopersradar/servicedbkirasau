﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Сектора РР
    /// </summary>
    [DataContract]
    [CategoryOrder("Сектор", 1)]
    [CategoryOrder("Прочее", 2)]
    [InfoTable(NameTable.TableSectorsRecon)]
    public class TableSectorsRecon : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AngleMin))]
        [PropertyOrder(1)]
        public short AngleMin { get; set; } 

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AngleMax))]
        [PropertyOrder(2)]
        public short AngleMax { get; set; }

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AntennaIndexStart)), Browsable(false)]
        [PropertyOrder(3)]
        public int AntennaIndexStart { get; set; }

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AntennaIndexEnd)), Browsable(false)]
        [PropertyOrder(4)]
        public int AntennaIndexEnd { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public String Note { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }

        public TableSectorsRecon() { }
        public TableSectorsRecon(short angleMin , short angleMax, int antennaIndexStart, int antennaIndexEnd, string note, bool isActive, int id = 0)
        {
            AngleMin = angleMin;
            AngleMax = angleMax;
            AntennaIndexStart = antennaIndexStart;
            AntennaIndexEnd = antennaIndexEnd;
            Note = note;
            IsActive = isActive;
            Id = id;
        }

        public TableSectorsRecon Clone() => new TableSectorsRecon(AngleMin, AngleMax, AntennaIndexStart, AntennaIndexEnd, Note, IsActive, Id);
        
        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableSectorsRecon)record;
            AngleMin = newRecord.AngleMin;
            AngleMax = newRecord.AngleMax;
            AntennaIndexStart = newRecord.AntennaIndexStart;
            AntennaIndexEnd = newRecord.AntennaIndexEnd;
            Note = newRecord.Note;
            IsActive = newRecord.IsActive;
        }
    }
}
