﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// УПП 
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(Coordinates), 3)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableRemotePoints)]
    public class TableRemotePoints : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id { get; set; }


        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(CourseAngle))]
        public float CourseAngle { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TimeError))]
        public short TimeError { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(IpAddress))]
        public string IpAddress { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Port))]
        public int Port { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; }


        public TableRemotePoints() { }
        public TableRemotePoints(Coord coord, float courseAngle, short timeError, string ipAddress, int port, string note, int id = 0)
        {
            Coordinates = coord.Clone();
            CourseAngle = courseAngle;
            TimeError = timeError;
            IpAddress = ipAddress;
            Port = port;
            Note = note;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableRemotePoints)record;

            Coordinates.Update(newRecord.Coordinates);
            CourseAngle = newRecord.CourseAngle;
            TimeError = newRecord.TimeError;
            IpAddress = newRecord.IpAddress;
            Port = newRecord.Port;
            Note = newRecord.Note;
        }

        public TableRemotePoints Clone() => new TableRemotePoints(Coordinates, CourseAngle, TimeError, IpAddress, Port, Note, Id);
    }
}
