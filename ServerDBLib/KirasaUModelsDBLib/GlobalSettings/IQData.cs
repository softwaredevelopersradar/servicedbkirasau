﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using YamlDotNet.Serialization;

namespace KirasaUModelsDBLib
{
    [DataContract]
    public class IQData : INotifyPropertyChanged, IDataErrorInfo
    {
        #region IModelMethods

        public IQData() { }
        public IQData(float timeRecord, string folderNB, Recorder recorder, CaptureMode captureMode, BufferSizes inputBufferSize, BufferSizes outputBufferSize, bool waitForStrobe)
        {
            TimeRecord = timeRecord;
            PathFolderNbFiles = folderNB;
            Recorder = recorder;
            CaptureMode = captureMode;
            InputBuffer = inputBufferSize;
            OutputBuffer = outputBufferSize;
            WaitForStrobe = waitForStrobe;
        }

        public IQData Clone() => new IQData(TimeRecord, PathFolderNbFiles, Recorder, CaptureMode, InputBuffer, OutputBuffer, WaitForStrobe);


        public bool EqualTo(IQData model)
        {
            return Recorder == model.Recorder
                && CaptureMode == model.CaptureMode
                && InputBuffer == model.InputBuffer
                && OutputBuffer == model.OutputBuffer
                && WaitForStrobe == model.WaitForStrobe
                && TimeRecord == model.TimeRecord
                && PathFolderNbFiles == model.PathFolderNbFiles;
        }

        public void Update(IQData model)
        {
            Recorder = model.Recorder;
            CaptureMode = model.CaptureMode;
            InputBuffer = model.InputBuffer;
            OutputBuffer = model.OutputBuffer;
            WaitForStrobe = model.WaitForStrobe;
            TimeRecord = model.TimeRecord;
            PathFolderNbFiles = model.PathFolderNbFiles;
        }
        #endregion

        #region IDataErrorInfo
        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }
        #endregion


        private float _timeRecord;
        private string _pathFolderNbFiles = "";
        private Recorder _recorder = Recorder.Master;
        private CaptureMode _captureMode = CaptureMode.Auto;
        private BufferSizes _inputBuffer = BufferSizes.x32;
        private BufferSizes _outputBuffer = BufferSizes.x32;
        private bool _waitForStrobe = true;

        [DataMember]
        [Range(0.1, 10)]
        [NotifyParentProperty(true)]
        public float TimeRecord
        {
            get => _timeRecord;
            set
            {
                if (_timeRecord == value) return;
                _timeRecord = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public string PathFolderNbFiles
        {
            get => _pathFolderNbFiles;
            set
            {
                if (_pathFolderNbFiles == value) return;
                _pathFolderNbFiles = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public Recorder Recorder
        {
            get => _recorder;
            set
            {
                if (_recorder == value) return;
                _recorder = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public CaptureMode CaptureMode
        {
            get => _captureMode;
            set
            {
                if (_captureMode == value) return;
                _captureMode = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public BufferSizes InputBuffer
        {
            get => _inputBuffer;
            set
            {
                if (_inputBuffer == value) return;
                _inputBuffer = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public BufferSizes OutputBuffer
        {
            get => _outputBuffer;
            set
            {
                if (_outputBuffer == value) return;
                _outputBuffer = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public bool WaitForStrobe
        {
            get => _waitForStrobe;
            set
            {
                if (_waitForStrobe == value) return;
                _waitForStrobe = value;
                OnPropertyChanged();
            }
        }


        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}
