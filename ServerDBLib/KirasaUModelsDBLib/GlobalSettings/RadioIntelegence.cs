﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using YamlDotNet.Serialization;

namespace KirasaUModelsDBLib
{
    [DataContract]
    public class RadioIntelegence : INotifyPropertyChanged, IDataErrorInfo
    {
        #region IModelMethods

        public RadioIntelegence() { }
        public RadioIntelegence(float courseAngle, SynchronizeTypes synchronizeType, float narrowBand, 
            float durationMin, float durationMax, float periodMin, float periodMax, byte reviewsCount, DeviceAnalysisTypes deviceAnalysisTypes)
        {
            CourseAngle = courseAngle;
            SynchronizeType = synchronizeType;
            NarrowBand = narrowBand;
            DurationMin = durationMin;
            DurationMax = durationMax;
            PeriodMin = periodMin;
            PeriodMax = periodMax;
            NumberOfReviews = reviewsCount;
            DeviceAnalysisType = deviceAnalysisTypes;
        }

        public RadioIntelegence Clone() => new RadioIntelegence
            (CourseAngle, SynchronizeType, NarrowBand, DurationMin, DurationMax, PeriodMin, PeriodMax, NumberOfReviews, this.DeviceAnalysisType);
        

        public bool EqualTo(RadioIntelegence model)
        {
            return CourseAngle == model.CourseAngle
                && SynchronizeType == model.SynchronizeType
                && NarrowBand == model.NarrowBand
                && DurationMin == model.DurationMin
                && DurationMax == model.DurationMax
                && PeriodMin == model.PeriodMin
                && PeriodMax == model.PeriodMax
                && NumberOfReviews == model.NumberOfReviews
                && this.DeviceAnalysisType == model.DeviceAnalysisType;
        }

        public void Update(RadioIntelegence model)
        {
            CourseAngle = model.CourseAngle;
            SynchronizeType = model.SynchronizeType;
            NarrowBand = model.NarrowBand;
            DurationMin = model.DurationMin;
            DurationMax = model.DurationMax;
            PeriodMin = model.PeriodMin;
            PeriodMax = model.PeriodMax;
            NumberOfReviews = model.NumberOfReviews;
            this.DeviceAnalysisType = model.DeviceAnalysisType;
        }
        #endregion

        #region IDataErrorInfo
        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }
        #endregion



        private float _courseAngle;
        private SynchronizeTypes _synchronizeType = SynchronizeTypes.GPS;
        private float _narrowBand;
        private float _durationMin;
        private float _durationMax;
        private float _periodMin;
        private float _periodMax;
        private byte _numberOfReviews;
        private DeviceAnalysisTypes deviceAnalysisType = DeviceAnalysisTypes.Master;

        [DataMember]
        [Range(0, 359)]
        [NotifyParentProperty(true)]
        public float CourseAngle
        {
            get => _courseAngle;
            set
            {
                if (_courseAngle == value) return;
                _courseAngle = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public SynchronizeTypes SynchronizeType
        {
            get => _synchronizeType;
            set
            {
                if (_synchronizeType == value) return;
                _synchronizeType = value;
                OnPropertyChanged();
            }
        }



        [DataMember]
        [Browsable(false)]
        [NotifyParentProperty(true)]
        public float NarrowBand
        {
            get => _narrowBand;
            set
            {
                if (_narrowBand == value) return;
                _narrowBand = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float DurationMin
        {
            get => _durationMin;
            set
            {
                if (_durationMin == value) return;
                _durationMin = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float DurationMax
        {
            get => _durationMax;
            set
            {
                if (_durationMax == value) return;
                _durationMax = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float PeriodMin
        {
            get => _periodMin;
            set
            {
                if (_periodMin == value) return;
                _periodMin = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float PeriodMax
        {
            get => _periodMax;
            set
            {
                if (_periodMax == value) return;
                _periodMax = value;
                OnPropertyChanged();
            }
        }
        

        [DataMember]
        [Range(4, 10)]
        [NotifyParentProperty(true)]
        public byte NumberOfReviews
        {
            get => _numberOfReviews;
            set
            {
                if (_numberOfReviews == value) return;
                _numberOfReviews = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public DeviceAnalysisTypes DeviceAnalysisType
        {
            get => this.deviceAnalysisType;
            set
            {
                if(this.deviceAnalysisType == value) return;
                this.deviceAnalysisType = value;
                this.OnPropertyChanged();
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}
