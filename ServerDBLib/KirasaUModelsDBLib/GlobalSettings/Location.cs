﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using YamlDotNet.Serialization;

namespace KirasaUModelsDBLib
{
    public class Location : INotifyPropertyChanged, IDataErrorInfo
    {

        #region IDataErrorInfo
        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }
        #endregion

        #region Model Methods

        public Location() { }
        public Location(CoordinateSystem coordinateSystem, double latitude, double longitude, float altitude, float courseAngle, float compassAngle)
        {
            CoordinateSystem = coordinateSystem;
            Latitude = latitude;
            Longitude = longitude;
            Altitude = altitude;
            CourseAngle = courseAngle;
            CompassAngle= compassAngle;
        }

        public bool EqualTo(Location model)
        {
            return CoordinateSystem == model.CoordinateSystem
                && Longitude == model.Longitude
                && Latitude == model.Latitude
                && Altitude == model.Altitude
                && CourseAngle == model.CourseAngle
                && CompassAngle == model.CompassAngle;
        }

        public Location Clone() =>  new Location(CoordinateSystem, Latitude, Longitude, Altitude, CourseAngle, CompassAngle);

        public void Update(Location model)
        {
            CoordinateSystem = model.CoordinateSystem;
            Longitude = model.Longitude;
            Latitude = model.Latitude;
            Altitude = model.Altitude;
            CourseAngle = model.CourseAngle;
            CompassAngle = model.CompassAngle;
        }

        #endregion


        private CoordinateSystem _coordinateSystem;
        private double _latitude = -1;
        private double _longitude = -1;
        private float _altitude = -1;
        private float _courseAngle;
        private float _compassAngle;

        [DataMember]
        [NotifyParentProperty(true)]
        public CoordinateSystem CoordinateSystem
        {
            get => _coordinateSystem;
            set
            {
                if (_coordinateSystem == value) return;
                _coordinateSystem = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public double Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public double Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float Altitude
        {
            get => _altitude;
            set
            {
                if (_altitude == value) return;
                _altitude = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float CourseAngle
        {
            get => _courseAngle;
            set
            {
                if (_courseAngle == value) return;
                _courseAngle = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float CompassAngle
        {
            get => _compassAngle;
            set
            {
                if (_compassAngle == value) return;
                _compassAngle = value;
                OnPropertyChanged();
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}
