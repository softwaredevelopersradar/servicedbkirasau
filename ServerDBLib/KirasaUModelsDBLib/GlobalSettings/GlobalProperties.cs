﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.GlobalProperties)]
    #region CategoryOrder
    //[CategoryOrder("RadioIntelegence", 1)]
    //[CategoryOrder("Calibration", 2)]
    //[CategoryOrder("Location", 3)]
    //[CategoryOrder("IQData", 4)]
    #endregion

    public class GlobalProperties : AbstractCommonTable, INotifyPropertyChanged
    {
        public GlobalProperties()
        {
            
        }

        private Location _location = new Location();
        //private Coord _location = new Coord();
        //private Calibration _calibration = new Calibration();
        //private RadioIntelegence _radioIntelegence = new RadioIntelegence();
        //private IQData _iqData = new IQData();


        [DataMember]
        [Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("Location")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Location Location 
            {
            get => _location;
            set
            {
                if (_location == value) return;
                _location = value;
                OnPropertyChanged();
    }
}




//[DataMember]
//[Category(nameof(RadioIntelegence))]
//[TypeConverter(typeof(ExpandableObjectConverter))]
//public RadioIntelegence RadioIntelegence 
//{
//    get => _radioIntelegence;
//    set
//    {
//        if (_radioIntelegence == value) return;
//        _radioIntelegence = value;
//        OnPropertyChanged();
//    }
//}

//[DataMember]
//[Category("Calibration")]
//[TypeConverter(typeof(ExpandableObjectConverter))]
//public Calibration Calibration
//{
//    get => _calibration;
//    set
//    {
//        if (_calibration == value) return;
//        _calibration = value;
//        OnPropertyChanged();
//    }
//}

//[DataMember]
//[Category("Location")]
//[TypeConverter(typeof(ExpandableObjectConverter))]
//public Coord Location
//{
//    get => _location;
//    set
//    {
//        if (_location == value) return;
//        _location = value;
//        OnPropertyChanged();
//    }
//}


//[DataMember]
//[Category("IQData")]
//[TypeConverter(typeof(ExpandableObjectConverter))]
//public IQData IQData
//{
//    get => _iqData;
//    set
//    {
//        if (_iqData == value) return;
//        _iqData = value;
//        OnPropertyChanged();
//    }
//}


#region IModelMethods

public GlobalProperties Clone()
        {
            return new GlobalProperties
            {
                Id = Id,
                //RadioIntelegence = RadioIntelegence.Clone(),
                //Calibration = Calibration.Clone(),
                //Location = new Coord(Location.Latitude, Location.Longitude, Location.Altitude),
                //IQData = IQData.Clone()
            };
        }

        public bool EqualTo(GlobalProperties model)
        {
            return Location == model.Location;
                //RadioIntelegence.EqualTo(model.RadioIntelegence)
                //&& Calibration.EqualTo(model.Calibration)
                //&& IQData.EqualTo(model.IQData)
        }

        public override void Update(AbstractCommonTable model)
        {
            var newModel = (GlobalProperties)model;
            
            Location.Update(newModel.Location);
            //RadioIntelegence.Update(newModel.RadioIntelegence);
            //Calibration.Update(newModel.Calibration);
            //IQData.Update(newModel.IQData);
        }

        #endregion


        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        #endregion
    }
}
