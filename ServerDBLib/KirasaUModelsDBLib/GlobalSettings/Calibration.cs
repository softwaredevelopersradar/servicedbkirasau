﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using YamlDotNet.Serialization;

namespace KirasaUModelsDBLib
{
    [DataContract]
    public class Calibration : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Model Methods

        public bool EqualTo(Calibration model)
        {
            return Type == model.Type;
        }

        public Calibration Clone()
        {
            return new Calibration
            {
                Type = Type
            };
        }

        public void Update(Calibration model)
        {
            Type = model.Type;
        }

        #endregion

        private CalibrationTypes _type = CalibrationTypes.Generator;

        [DataMember]
        [NotifyParentProperty(true)]
        public CalibrationTypes Type
        {
            get => _type;
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged();
            }
        }


        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion


        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion
    }
}
