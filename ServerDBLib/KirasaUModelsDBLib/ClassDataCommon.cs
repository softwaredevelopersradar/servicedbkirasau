﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace KirasaUModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(TableFreqRangesRecon))]
    [KnownType(typeof(TableFreqForbidden))]
    [KnownType(typeof(TableSectorsRecon))]
    [KnownType(typeof(TableRes))]
    [KnownType(typeof(TableResArchive))]
    [KnownType(typeof(TableResDistribution))]
    [KnownType(typeof(TableResPattern))]
    [KnownType(typeof(GlobalProperties))]
    [KnownType(typeof(TableTrackArchive))]
    [KnownType(typeof(TableTrackDistribution))]
    [KnownType(typeof(TableRemotePoints))]

    public class ClassDataCommon
    {
        [DataMember]
        public List<AbstractCommonTable> ListRecords { get; set; }

        public ClassDataCommon()
        {
            ListRecords = new List<AbstractCommonTable>();
        }

        public List<T> ToList<T>() where T : class
        {
            return (from t in ListRecords let c = t as T select c).ToList();
        }

        public static ClassDataCommon ConvertToListAbstractCommonTable<T>(List<T> listRecords) where T : class
        {
            ClassDataCommon objListAbstractData = new ClassDataCommon();
            if (listRecords == null)
                return null;
            if (listRecords.Count == 0)
                return objListAbstractData;
            objListAbstractData.ListRecords = (from t in listRecords let c = t as AbstractCommonTable select c).ToList();
            return objListAbstractData;
        }
    }
}
