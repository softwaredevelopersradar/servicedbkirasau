﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// Шаблоны ИРИ 
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(ParamDP), 3)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableResPattern)]
    public class TableResPattern : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyMinKHz))]
        public double FrequencyMinKHz { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyMaxKHz))]
        public double FrequencyMaxKHz { get; set; }


        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse { get; set; }


        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Series { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Name))]
        public string Name { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(ModulationType))]
        public ModulationType ModulationType { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(WorkingMode))]
        public WorkingMode WorkingMode { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TargetClass))]
        public ClassTarget TargetClass { get; set; }


        public TableResPattern() { }
        public TableResPattern(double frequencyMinKhz, double frequencyMaxKhz, ParamDP pulse, ParamDP series, string name, ModulationType modulationType, WorkingMode workingMode, ClassTarget targetClass, int id = 0)
        {
            FrequencyMinKHz = frequencyMinKhz;
            FrequencyMaxKHz = frequencyMaxKhz;
            Pulse = pulse.Clone();
            Series = series.Clone();
            Name = name;
            ModulationType = modulationType;
            WorkingMode = workingMode;
            TargetClass = targetClass;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResPattern)record;
            FrequencyMinKHz = newRecord.FrequencyMinKHz;
            FrequencyMaxKHz = newRecord.FrequencyMaxKHz;
            Pulse.Update(newRecord.Pulse);
            Series.Update(newRecord.Series);
            Name = newRecord.Name;
            ModulationType = newRecord.ModulationType;
            WorkingMode = newRecord.WorkingMode;
            TargetClass = newRecord.TargetClass;
        }

        public TableResPattern Clone() => new TableResPattern(FrequencyMinKHz, FrequencyMaxKHz, Pulse, Series, Name, ModulationType, WorkingMode, TargetClass, Id);
    }
}
