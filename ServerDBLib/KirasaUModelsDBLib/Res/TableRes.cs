﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// ИРИ 
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(ParamDP), 3)]
    [CategoryOrder("Прочее", 4)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableRes)]
    public class TableRes : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyKHz))]
        public double FrequencyKHz { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Bearing))]
        public float Bearing { get; set; }


        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse { get; set; }


        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Series { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TargetType))]
        public string TargetType { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(ModulationType))]
        public ModulationType ModulationType { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaMaster))]
        public byte AntennaMaster { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaSlave))]
        public byte AntennaSlave { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AmplitudeMaster))]
        public double AmplitudeMaster { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AmplitudeSlave))]
        public double AmplitudeSlave { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; }


        public TableRes() { }
        public TableRes(double frequencyKhz, float bearing, ParamDP pulse, ParamDP series, string targetType, ModulationType modulationType, byte antennaMaster, byte antennaSlave, double amplitudeMaster, double amplitudeSlave, string note, int id = 0) 
        {
            FrequencyKHz = frequencyKhz;
            Bearing = bearing;
            Pulse = pulse.Clone();
            Series = series.Clone();
            TargetType = targetType;
            ModulationType = modulationType;
            AntennaMaster = antennaMaster;
            AntennaSlave = antennaSlave;
            AmplitudeMaster = amplitudeMaster;
            AmplitudeSlave = amplitudeSlave;
            Note = note;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableRes)record;
            FrequencyKHz = newRecord.FrequencyKHz;
            Bearing = newRecord.Bearing;
            Pulse.Update(newRecord.Pulse);
            Series.Update(newRecord.Series);
            TargetType = newRecord.TargetType;
            ModulationType = newRecord.ModulationType;
            AntennaMaster = newRecord.AntennaMaster;
            AntennaSlave = newRecord.AntennaSlave;
            AmplitudeMaster = newRecord.AmplitudeMaster;
            AmplitudeSlave = newRecord.AmplitudeSlave;
            Note = newRecord.Note;
        }

        public TableRes Clone() => new TableRes(FrequencyKHz, Bearing, Pulse, Series, TargetType, ModulationType, AntennaMaster, AntennaSlave, AmplitudeMaster, AmplitudeSlave, Note, Id);
    }
}
