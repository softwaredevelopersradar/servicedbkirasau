﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// ИРИ ЦР
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(ParamDP), 3)]
    [CategoryOrder("Прочее", 4)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableResDistribution)]
    public class TableResDistribution : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyKHz))]
        public double FrequencyKHz { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Bearing))]
        public float Bearing { get; set; }


        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse { get; set; }


        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Series { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TargetType))]
        public string TargetType { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaMaster))]
        public byte AntennaMaster { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaSlave))]
        public byte AntennaSlave { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(ReceiverL))]
        public ReceiveSignal ReceiverL { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Receiver1))]
        public ReceiveSignal Receiver1 { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Receiver2))]
        public ReceiveSignal Receiver2 { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Receiver3))]
        public ReceiveSignal Receiver3 { get; set; }


        [DataMember]
        public ObservableCollection<TableTrackDistribution> Track { get; set; } = new ObservableCollection<TableTrackDistribution>();


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; }


        public TableResDistribution() { }
        public TableResDistribution(double frequencyKhz, float bearing, ParamDP pulse, ParamDP series, string targetType, byte antennaMaster, byte antennaSlave, ReceiveSignal l, ReceiveSignal r1, ReceiveSignal r2, ReceiveSignal r3, ObservableCollection<TableTrackDistribution> track, string note, int id = 0)
        {
            FrequencyKHz = frequencyKhz;
            Bearing = bearing;
            Pulse = pulse.Clone();
            Series = series.Clone();
            TargetType = targetType;
            AntennaMaster = antennaMaster;
            AntennaSlave = antennaSlave;
            ReceiverL = l;
            Receiver1 = r1;
            Receiver2 = r2;
            Receiver2 = r3;
            Track  = new ObservableCollection<TableTrackDistribution>(track);
            Note = note;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResDistribution)record;
            FrequencyKHz = newRecord.FrequencyKHz;
            Bearing = newRecord.Bearing;
            Pulse.Update(newRecord.Pulse);
            Series.Update(newRecord.Series);
            TargetType = newRecord.TargetType;
            AntennaMaster = newRecord.AntennaMaster;
            AntennaSlave = newRecord.AntennaSlave;
            ReceiverL = newRecord.ReceiverL;
            Receiver1 = newRecord.Receiver1;
            Receiver2 = newRecord.Receiver2;
            Receiver3 = newRecord.Receiver3;
            Note = newRecord.Note;

            Track = Track ?? newRecord.Track;
            if (Track != newRecord.Track)
            {
                Track.Clear();
                foreach (var point in newRecord.Track)
                    Track.Add(point);
            }
        }

        public TableResDistribution Clone() => new TableResDistribution(FrequencyKHz, Bearing, Pulse, Series, TargetType, AntennaMaster, AntennaSlave, ReceiverL, Receiver1, Receiver2, Receiver3, Track, Note, Id);
    }
}
