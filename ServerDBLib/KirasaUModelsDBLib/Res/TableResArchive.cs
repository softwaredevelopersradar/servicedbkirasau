﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaUModelsDBLib
{
    /// <summary>
    /// ИРИ Архив
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(ParamDP), 3)]
    [CategoryOrder("Прочее", 4)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableResArchive)]
    public class TableResArchive: AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyKHz))]
        public double FrequencyKHz { get; set; }


        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse { get; set; }


        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Series { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TargetType))]
        public string TargetType { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaMaster))]
        public byte AntennaMaster { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaSlave))]
        public byte AntennaSlave { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(DateTimeStart))]
        public DateTime DateTimeStart { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(DateTimeStop))]
        public DateTime DateTimeStop { get; set; }


        [DataMember]
        public ObservableCollection<TableTrackArchive> Track { get; set; } = new ObservableCollection<TableTrackArchive>();


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; }


        public TableResArchive() { }
        public TableResArchive(double frequencyKhz, ParamDP pulse, ParamDP series, string targetType, byte antennaMaster, byte antennaSlave, DateTime dateTimeStart, DateTime dateTimeStop, ObservableCollection<TableTrackArchive> track, string note, int id = 0)
        {
            FrequencyKHz = frequencyKhz;
            Pulse = pulse.Clone();
            Series = series.Clone();
            TargetType = targetType;
            AntennaMaster = antennaMaster;
            AntennaSlave = antennaSlave;
            DateTimeStart = dateTimeStart;
            DateTimeStop = dateTimeStop;
            Track = new ObservableCollection<TableTrackArchive>(track);
            Note = note;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResArchive)record;
            FrequencyKHz = newRecord.FrequencyKHz;
            Pulse.Update(newRecord.Pulse);
            Series.Update(newRecord.Series);
            TargetType = newRecord.TargetType;
            AntennaMaster = newRecord.AntennaMaster;
            AntennaSlave = newRecord.AntennaSlave;
            DateTimeStart = newRecord.DateTimeStart;
            DateTimeStop = newRecord.DateTimeStop;

            
            Note = newRecord.Note;

            Track = Track ?? newRecord.Track;
            if (Track != newRecord.Track)
            {
                Track.Clear();
                foreach (var point in newRecord.Track)
                    Track.Add(point);
            }
        }

        public TableResArchive Clone() => new TableResArchive(FrequencyKHz, Pulse, Series, TargetType, AntennaMaster, AntennaSlave, DateTimeStart, DateTimeStop, Track, Note, Id);
    }
}
